﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Interfaces;
using TecwiEmployees.Enums;
using TecwiEmployees.Models;

namespace TecwiEmployees.Services
{
    public class FactoryService
    {
        public Worker CreateWorker(WorkerTypes type, string name, DateTime date, IWorkerService workerService)
        {
            switch (type)
            {
                case WorkerTypes.Employee: return new Employee(name, date, workerService);
                case WorkerTypes.Manager: return new Manager(name, date, workerService);
                case WorkerTypes.Sales: return new Sales(name, date, workerService);
                default: return null;
            }
        }

        public List<Worker> CreateWorkers(WorkerTypes type, string name, DateTime date, IWorkerService workerService, int quantity)
        {
            var workers = new List<Worker>();

            for (int i = 0; i < quantity; i++) {
                workers.Add(CreateWorker(type, name+quantity, date, workerService));
            }

            return workers;
        }
    }
}
