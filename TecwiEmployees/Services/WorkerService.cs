﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Helpers;
using TecwiEmployees.Interfaces;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Models;

namespace TecwiEmployees.Services
{
    public class WorkerService : IWorkerService
    {
        public decimal CalculateAnnualBonus(decimal baseRate, decimal maxAnnualBonusSum, decimal annualBonusRate, DateTime calculationDate, DateTime employmentDate)
        {
            decimal bonus = 0;

            if (calculationDate > employmentDate)
            {
                var maxBonus = baseRate * maxAnnualBonusSum;
                var workYears = DateTimeHelper.GetTotalYears(calculationDate - employmentDate);
                bonus = workYears * (baseRate * annualBonusRate);
                bonus = bonus > maxBonus ? maxBonus : bonus;
            }
            
            return bonus;
        }

        public SalaryData CalculateSalaryData(DateTime calculationDate, Dictionary<Guid, Worker> subordinates)
        {
            var subSalaryTotal = new SalaryData();

            foreach (var pair in subordinates)
            {
                var subSalary = pair.Value.CalculateSubordinatesSalaryData(calculationDate);
                subSalaryTotal.SalarySum += subSalary.SalarySum;
                subSalaryTotal.Salary += subSalary.Salary;
            }

            return subSalaryTotal;
        }

        public bool IsNotMyBoss(Guid employerId, BossWorker toHire)
        {
            bool canHire = true;

            if (toHire.GetSubordinates().ContainsKey(employerId))
            {
                canHire = false;
            }
            else
            {
                foreach (var kvPair in toHire.GetSubordinates())
                {
                    if (canHire == false) break;
                    canHire = kvPair.Value.CanBeEmployed(employerId);

                }
            }

            return canHire;
        }
    }
}
