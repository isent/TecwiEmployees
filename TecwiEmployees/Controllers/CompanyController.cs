﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Models;

namespace TecwiEmployees.Controllers
{
    public class CompanyController
    {
        public decimal GetCompanyTotalSalary(Company company, DateTime calculationDate)
        {
            return company.GetTotalSalary(calculationDate);
        }
    }
}
