﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Interfaces;
using TecwiEmployees.Abstractions;

namespace TecwiEmployees.Controllers
{
    public class WorkerController
    {
        public decimal GetWorkerSalary(Worker worker, DateTime calculationDate)
        {
            return worker.CalculateSalary(calculationDate);
        }
    }
}
