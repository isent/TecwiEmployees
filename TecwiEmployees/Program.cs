﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Models;
using TecwiEmployees.Services;
using TecwiEmployees.Enums;
using TecwiEmployees.Controllers;

namespace TecwiEmployees
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"-------------- This is just demo - use tests! --------------{Environment.NewLine}");

            var workerSvc = new WorkerService();
            var factoryService = new FactoryService();
            var date = new DateTime(2000, 1, 1);

            var company = new Company();
            var manager0 = new Manager("Manager0", date, workerSvc);
            var sales0 = new Sales("Sales0", date, workerSvc);
            var employees = factoryService.CreateWorkers(WorkerTypes.Employee, "Employee", date, workerSvc, 10).Cast<Employee>().ToList();

            manager0.AddSubordinate(new Manager("Manager01", date, workerSvc));
            sales0.AddSubordinate(new Employee("Employee01", date, workerSvc));
            company.Workers.Add(manager0);
            company.Workers.Add(sales0);
            company.Workers.AddRange(employees);

            Console.WriteLine($"Stats for period {date} - {DateTime.Now}:");
            Console.WriteLine($"Total company salary: {company.GetTotalSalary(DateTime.Now)}");

            foreach (var worker in company.Workers)
            {
                Console.WriteLine($"{worker.Name} salary: {worker.CalculateSalary(DateTime.Now)}");
            }

            Console.ReadLine();
        }
    }
}
