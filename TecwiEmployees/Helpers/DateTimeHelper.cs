﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecwiEmployees.Helpers
{
    public static class DateTimeHelper
    {
        private const float GregorianMeanYear = 365.2425f;

        public static int GetTotalYears(TimeSpan timeSpan)
        {
            return (int)(timeSpan.TotalDays / GregorianMeanYear);
        }
    }
}
