﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Services;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Interfaces;

namespace TecwiEmployees.Models
{
    public class Employee : CommonWorker
    {
        public Employee() : this(null, new DateTime(), new WorkerService())
        {
        }

        public Employee(string name, DateTime employmentDate, IWorkerService workerService) : base(name, employmentDate, 0.03m, 0.3m, workerService)
        {
        }

        public override decimal CalculateSalary(DateTime calculationDate)
        {
            decimal salary = 0;

            if (calculationDate > EmploymentDate)
            {
                salary = _workerService.CalculateAnnualBonus(BaseRate, MaxAnnualBonusSum, AnnualBonusRate, calculationDate, EmploymentDate) + BaseRate;
            }

            return salary;
        }

        public override bool CanBeEmployed(Guid employerId)
        {
            return true;
        }

        public override SalaryData CalculateSubordinatesSalaryData(DateTime calculationDate)
        {
            var salary = CalculateSalary(calculationDate);
            return new SalaryData(salary, salary);
        }
    }
}
