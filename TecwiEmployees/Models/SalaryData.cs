﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecwiEmployees.Models
{
    public class SalaryData
    {
        public decimal Salary {get; set;}

        public decimal SalarySum { get; set; }

        public SalaryData()
        {
        }

        public SalaryData(decimal salary, decimal salarySum)
        {
            Salary = salary;
            SalarySum = salarySum;
        }
    }
}
