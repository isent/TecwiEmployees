﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Services;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Interfaces;

namespace TecwiEmployees.Models
{
    public class Sales : BossWorker
    {
        public Sales() : this(null, new DateTime(), new WorkerService())
        {
        }

        public Sales(string name, DateTime employmentDate, IWorkerService workerService) : base(name, employmentDate, 0.01m, 0.35m, 0.003m, workerService)
        {
        }

        public override decimal CalculateSalary(DateTime calculationDate)
        {
            decimal salary = 0;

            if (calculationDate > EmploymentDate)
            {
                var salaryModel = CalculateSubordinatesSalaryData(calculationDate);
                salary = salaryModel.Salary;
            }

            return salary;
        }

        public override bool CanBeEmployed(Guid employerId)
        {
            bool canHire = true;
            canHire = _workerService.IsNotMyBoss(employerId, this);

            return canHire;
        }

        public override SalaryData CalculateSubordinatesSalaryData(DateTime calculationDate)
        {
            var mySalary = 0m;
            var annualBonus = _workerService.CalculateAnnualBonus(BaseRate, MaxAnnualBonusSum, AnnualBonusRate, calculationDate, EmploymentDate);
            var subSalaryTotal = _workerService.CalculateSalaryData(calculationDate, GetSubordinates());

            mySalary = subSalaryTotal.SalarySum * PerSubordinateBonusRate + annualBonus + BaseRate;
            subSalaryTotal.Salary = mySalary;
            subSalaryTotal.SalarySum += mySalary;

            return subSalaryTotal;
        }
    }
}
