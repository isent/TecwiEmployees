﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Interfaces;
using TecwiEmployees.Abstractions;

namespace TecwiEmployees.Models
{
    public class Company
    {
        public List<Worker> Workers { get; set; }

        public Company()
        {
            Workers = new List<Worker>();
        }

        public decimal GetTotalSalary(DateTime calculationDate)
        {
            return Workers.Sum(w => w.CalculateSalary(calculationDate));
        }
    }
}
