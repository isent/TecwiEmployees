﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Interfaces;
using TecwiEmployees.Models;

namespace TecwiEmployees.Abstractions
{
    public abstract class Worker : IWorker, IEmployable
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime EmploymentDate { get; set; }

        public const decimal BaseRate = 1000;

        protected readonly IWorkerService _workerService;

        protected Worker()
        {
        }

        protected Worker(string name, DateTime employmentDate, IWorkerService workerService)
        {
            Id = Guid.NewGuid();
            Name = name;
            EmploymentDate = employmentDate;
            _workerService = workerService;
        }

        public abstract decimal CalculateSalary(DateTime calculationDate);

        public abstract bool CanBeEmployed(Guid employerId);

        public abstract SalaryData CalculateSubordinatesSalaryData(DateTime calculationDate);
    }
}
