﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Interfaces;

namespace TecwiEmployees.Abstractions
{
    public abstract class CommonWorker : Worker
    {
        public readonly decimal AnnualBonusRate;

        public readonly decimal MaxAnnualBonusSum;

        protected CommonWorker() : base()
        {
        }

        protected CommonWorker(string name, DateTime employmentDate, decimal annualBonusRate, decimal maxAnnualBonusSum, IWorkerService workerService) : 
            base(name, employmentDate, workerService)
        {
            AnnualBonusRate = annualBonusRate;
            MaxAnnualBonusSum = maxAnnualBonusSum;
        }
    }
}
