﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Interfaces;

namespace TecwiEmployees.Abstractions
{
    public abstract class BossWorker : CommonWorker
    {
        public decimal PerSubordinateBonusRate { get; set; }

        private Dictionary<Guid, Worker> _subordinates { get; set; }

        protected BossWorker() : base()
        {
        }

        protected BossWorker(string name, DateTime employmentDate, decimal annualBonusRate, decimal maxAnnualBonusSum, decimal perSubordinateBonusRate, IWorkerService workerService) : 
            base(name, employmentDate, annualBonusRate, maxAnnualBonusSum, workerService)
        {
            PerSubordinateBonusRate = perSubordinateBonusRate;
            _subordinates = new Dictionary<Guid, Worker>();
        }

        public virtual bool AddSubordinate(Worker worker)
        {
            var isAdded = false;

            if (!_subordinates.ContainsKey(worker.Id) && worker.CanBeEmployed(Id))
            {
                _subordinates.Add(worker.Id, worker);
                isAdded = true;
            }

            return isAdded;
        }

        public virtual Dictionary<Guid, Worker> GetSubordinates()
        {
            return _subordinates;
        }
    }
}
