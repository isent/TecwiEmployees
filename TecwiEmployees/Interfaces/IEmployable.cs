﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Abstractions;

namespace TecwiEmployees.Interfaces
{
    public interface IEmployable
    {
        bool CanBeEmployed(Guid employerId);
    }
}
