﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecwiEmployees.Interfaces
{
    public interface IWorker
    {
        decimal CalculateSalary(DateTime calculationDate);
    }
}
