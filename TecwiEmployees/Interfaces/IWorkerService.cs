﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Models;

namespace TecwiEmployees.Interfaces
{
    public interface IWorkerService
    {
        decimal CalculateAnnualBonus(decimal baseRate, decimal maxAnnualBonusSum, decimal annualBonusRate, DateTime calculationDate, DateTime employmentDate);

        /// <summary>
        /// Calculate first level subordinate salary and sum of all his subordinates salaries
        /// </summary>
        /// <param name="calculationDate"></param>
        /// <param name="subordinates"></param>
        /// <returns>Object with first level subordinate salary and sum of all his subordinates salaries</returns>
        SalaryData CalculateSalaryData(DateTime calculationDate, Dictionary<Guid, Worker> subordinates);

        /// <summary>
        /// Cheks whether trying to hire own boss
        /// </summary>
        /// <param name="employerId">Who wants to hire</param>
        /// <param name="worker">hiring target</param>
        /// <returns>Not trying to hire own boss</returns>
        bool IsNotMyBoss(Guid employerId, BossWorker toHire);
    }
}
