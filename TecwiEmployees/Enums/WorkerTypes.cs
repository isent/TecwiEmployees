﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecwiEmployees.Enums
{
    public enum WorkerTypes
    {
        Employee,
        Manager,
        Sales
    }
}
