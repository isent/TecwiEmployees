﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TecwiEmployees.Models;
using TecwiEmployees.Enums;
using TecwiEmployees.Services;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Interfaces;

namespace UnitTests.CompanyTests
{
    [TestFixture]
    public class SalaryTests
    {
        private FactoryService _factorySvc;
        private DateTime _employmentDate;
        private DateTime _calculationDate;
        private IWorkerService _workerSvc;

        public SalaryTests()
        {
            _factorySvc = new FactoryService();
            _employmentDate = new DateTime(2000, 1, 1);
            _calculationDate = new DateTime(2001, 1, 1);
            _workerSvc = new WorkerService();
        }

        [TestCase(WorkerTypes.Manager, WorkerTypes.Employee, new WorkerTypes[2] { WorkerTypes.Manager, WorkerTypes.Sales }, 2085.27525)]
        public void CompanyHasCorrectSalaryForNestedAndSibingWorkers(WorkerTypes rootType, WorkerTypes siblingType, WorkerTypes[] types, decimal result)
        {
            var rootWorker = _factorySvc.CreateWorker(rootType, "", _employmentDate, _workerSvc) as BossWorker;
            var prevWorker = rootWorker;

            foreach (var type in types)
            {
                var worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as BossWorker;
                prevWorker.AddSubordinate(worker);
                prevWorker = worker;
            }

            var company = new Company();
            company.Workers.Add(rootWorker);
            company.Workers.Add(_factorySvc.CreateWorker(siblingType, "", _employmentDate, _workerSvc) as Worker);
            var salary = company.GetTotalSalary(_calculationDate);
            Assert.AreEqual(result, salary);
        }
    }
}
