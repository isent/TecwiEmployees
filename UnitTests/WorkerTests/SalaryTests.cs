﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TecwiEmployees.Models;
using TecwiEmployees.Enums;
using TecwiEmployees.Services;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Interfaces;

namespace UnitTests.WorkerTests
{
    [TestFixture]
    public class SalaryTests
    {
        private FactoryService _factorySvc;
        private DateTime _employmentDate;
        private DateTime _calculationDate;
        private IWorkerService _workerSvc;

        public SalaryTests()
        {
            _factorySvc = new FactoryService();
            _employmentDate = new DateTime(2000, 1, 1);
            _calculationDate = new DateTime(2001, 1, 1);
            _workerSvc = new WorkerService();
        }

        [TestCase(WorkerTypes.Employee, 1030)]
        [TestCase(WorkerTypes.Manager, 1050),]
        [TestCase(WorkerTypes.Sales, 1010)]
        public void WorkerWithoutSubordinatesHasCorrectSalary(WorkerTypes type, decimal result)
        {
            IWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as IWorker;
            var salary = worker.CalculateSalary(_calculationDate);
            Assert.AreEqual(result, salary);
        }

        [TestCase(WorkerTypes.Employee, "1999, 1, 1", 0)]
        [TestCase(WorkerTypes.Manager, "1999, 1, 1", 0),]
        [TestCase(WorkerTypes.Sales, "1999, 1, 1", 0)]
        public void WorkerWithoutSubordinatesHasNoSalaryIfWorkPeriodIsNegative(WorkerTypes type, string calculationDate, decimal result)
        {
            var calcDate = DateTime.Parse(calculationDate);
            IWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as IWorker;
            var salary = worker.CalculateSalary(calcDate);
            Assert.AreEqual(result, salary);
        }

        [TestCase(WorkerTypes.Manager, WorkerTypes.Employee, 1055.15)]
        [TestCase(WorkerTypes.Sales, WorkerTypes.Employee, 1013.09)]
        public void WorkerWithSubordinateHasCorrectSalary(WorkerTypes type, WorkerTypes sType, decimal result)
        {
            BossWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as BossWorker;
            Worker subordinate = _factorySvc.CreateWorker(sType, "", _employmentDate, _workerSvc) as Worker;
            worker.AddSubordinate(subordinate);
            var salary = worker.CalculateSalary(_calculationDate);
            Assert.AreEqual(result, salary);
        }

        [TestCase(WorkerTypes.Manager, new WorkerTypes[2] { WorkerTypes.Employee , WorkerTypes.Employee }, 1060.3)]
        [TestCase(WorkerTypes.Sales, new WorkerTypes[2] { WorkerTypes.Employee, WorkerTypes.Employee }, 1016.18)]
        public void WorkerWithSubordinatesHasCorrectSalary(WorkerTypes type, WorkerTypes[] sTypes, decimal result)
        {
            BossWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as BossWorker;

            foreach (var sType in sTypes)
            {
                var subordinate = _factorySvc.CreateWorker(sType, "", _employmentDate, _workerSvc) as Worker;
                worker.AddSubordinate(subordinate);
            }

            var salary = worker.CalculateSalary(_calculationDate);
            Assert.AreEqual(result, salary);
        }

        [TestCase(WorkerTypes.Manager, new WorkerTypes[2] { WorkerTypes.Manager, WorkerTypes.Sales }, 1055.27525)]
        [TestCase(WorkerTypes.Sales, new WorkerTypes[2] {  WorkerTypes.Manager, WorkerTypes.Sales }, 1016.19515)]
        public void WorkerWithNestedSubordinatesHasCorrectSalary(WorkerTypes rootType, WorkerTypes[] types, decimal result)
        {
            var rootWorker = _factorySvc.CreateWorker(rootType, "", _employmentDate, _workerSvc) as BossWorker;
            var prevWorker = rootWorker;

            foreach (var type in types) 
            {
                var worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as BossWorker;
                prevWorker.AddSubordinate(worker);
                prevWorker = worker;
            }

            var salary = rootWorker.CalculateSalary(_calculationDate);
            Assert.AreEqual(result, salary);
        }

        [Test]
        public void WorkerWithDeepNestedSubordinatesHasCorrectSalary()
        {
            var employee = _factorySvc.CreateWorker(WorkerTypes.Employee, "Emp", _employmentDate, _workerSvc);
            var salesSub = _factorySvc.CreateWorker(WorkerTypes.Sales, "SalesSub", _employmentDate, _workerSvc) as BossWorker;
            var sales = _factorySvc.CreateWorker(WorkerTypes.Sales, "Sales", _employmentDate, _workerSvc) as BossWorker;

            salesSub.AddSubordinate(employee);
            sales.AddSubordinate(salesSub);
            var salary = sales.CalculateSalary(_calculationDate);
            Assert.AreEqual(1016.12927, salary);
        }

        [Test]
        public void WorkerWithDeepAndWidthNestedSubordinatesHasCorrectSalary()
        {
            var employee3 = _factorySvc.CreateWorker(WorkerTypes.Employee, "Emp3", _employmentDate, _workerSvc);
            var employee2 = _factorySvc.CreateWorker(WorkerTypes.Employee, "Emp2", _employmentDate, _workerSvc);
            var employee1 = _factorySvc.CreateWorker(WorkerTypes.Employee, "Emp1", _employmentDate, _workerSvc);
            var salesSub = _factorySvc.CreateWorker(WorkerTypes.Sales, "SalesSub", _employmentDate, _workerSvc) as BossWorker;
            var sales = _factorySvc.CreateWorker(WorkerTypes.Sales, "Sales", _employmentDate, _workerSvc) as BossWorker;

            salesSub.AddSubordinate(employee3);
            sales.AddSubordinate(employee1);
            sales.AddSubordinate(employee2);
            sales.AddSubordinate(salesSub);
            var salary = sales.CalculateSalary(_calculationDate);
            Assert.AreEqual(1022.30927, salary);
        }
    }
}
