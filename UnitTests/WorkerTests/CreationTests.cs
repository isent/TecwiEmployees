﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TecwiEmployees.Models;
using TecwiEmployees.Enums;
using TecwiEmployees.Services;
using TecwiEmployees.Abstractions;
using TecwiEmployees.Interfaces;

namespace UnitTests.WorkerTests
{
    [TestFixture]
    public class CreationTests
    {
        private FactoryService _factorySvc;
        private DateTime _employmentDate;
        private IWorkerService _workerSvc;

        public CreationTests()
        {
            _factorySvc = new FactoryService();
            _employmentDate = new DateTime(2000, 1, 1);
            _workerSvc = new WorkerService();
        }

        [TestCase(WorkerTypes.Employee, 0.03)]
        [TestCase(WorkerTypes.Manager, 0.05),]
        [TestCase(WorkerTypes.Sales, 0.01)]
        public void WorkerHasCorrectAnnualBonusRate(WorkerTypes type, decimal result)
        {
            CommonWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as CommonWorker;
            Assert.AreEqual(result, worker.AnnualBonusRate);
        }

        [TestCase(WorkerTypes.Employee, 0.3)]
        [TestCase(WorkerTypes.Manager, 0.4),]
        [TestCase(WorkerTypes.Sales, 0.35)]
        public void WorkerHasCorrectMaxAnnualBonusSum(WorkerTypes type, decimal result)
        {
            CommonWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as CommonWorker;
            Assert.AreEqual(result, worker.MaxAnnualBonusSum);
        }
 
        [TestCase(WorkerTypes.Manager, 0.005),]
        [TestCase(WorkerTypes.Sales, 0.003)]
        public void WorkerHasCorrectPerSubordinateBonusRate(WorkerTypes type, decimal result)
        {
            BossWorker worker = _factorySvc.CreateWorker(type, "", _employmentDate, _workerSvc) as BossWorker;
            Assert.AreEqual(result, worker.PerSubordinateBonusRate);
        }

        [TestCase(WorkerTypes.Manager, WorkerTypes.Manager, false)]
        [TestCase(WorkerTypes.Sales, WorkerTypes.Sales, false)]
        [TestCase(WorkerTypes.Sales, WorkerTypes.Manager, false)]
        [TestCase(WorkerTypes.Manager, WorkerTypes.Sales, false)]
        public void CannotHireOwnBoss(WorkerTypes bossType, WorkerTypes subordinateType, bool result)
        {
            BossWorker boss = _factorySvc.CreateWorker(bossType, "", _employmentDate, _workerSvc) as BossWorker;
            BossWorker subordinate = _factorySvc.CreateWorker(subordinateType, "", _employmentDate, _workerSvc) as BossWorker;
            boss.AddSubordinate(subordinate);
            var canHire = subordinate.AddSubordinate(boss);
            Assert.AreEqual(result, canHire);
        }

        [TestCase(WorkerTypes.Manager, WorkerTypes.Manager, false)]
        [TestCase(WorkerTypes.Manager, WorkerTypes.Sales, false)]
        public void WorkerCannotHaveDuplicateBossWorkerSubordinates(WorkerTypes bossType, WorkerTypes subordinateType, bool result)
        {
            BossWorker boss = _factorySvc.CreateWorker(bossType, "", _employmentDate, _workerSvc) as BossWorker;
            BossWorker subordinate = _factorySvc.CreateWorker(subordinateType, "", _employmentDate, _workerSvc) as BossWorker;
            boss.AddSubordinate(subordinate);
            var canHaveDuplicate = boss.AddSubordinate(subordinate);
            Assert.AreEqual(result, canHaveDuplicate);
        }

        [TestCase(WorkerTypes.Manager, WorkerTypes.Employee, false)]
        public void WorkerCannotHaveDuplicateEmployeeSubordinates(WorkerTypes bossType, WorkerTypes subordinateType, bool result)
        {
            BossWorker boss = _factorySvc.CreateWorker(bossType, "", _employmentDate, _workerSvc) as BossWorker;
            Employee subordinate = _factorySvc.CreateWorker(subordinateType, "", _employmentDate, _workerSvc) as Employee;
            boss.AddSubordinate(subordinate);
            var canHaveDuplicate = boss.AddSubordinate(subordinate);
            Assert.AreEqual(result, canHaveDuplicate);
        }
    }
}
